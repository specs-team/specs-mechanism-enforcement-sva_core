# About #
This repository contains shared functionality between [SVA_enforcement](https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_vulnerability_manager) and [SVA_monitoring](https://bitbucket.org/specs-team/specs-mechanism-monitoring-sva)

# Notice #

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).
* http://www.specs-project.eu/
* http://www.xlab.si/

Developers:

```
Aljaž Košir, aljaz.kosir@xlab.si
```

Copyright:

```
Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

E2EE client is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License, version 3,
as published by the Free Software Foundation.

E2EE client is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```