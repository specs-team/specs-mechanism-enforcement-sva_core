from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(name='specs_sva_core',
      version=version,
      description="utils",
      long_description="""\
utils""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='specs sva utils',
      author='Aljaz',
      author_email='aljaz.kosir@xlab.si',
      url='',
      license='',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=True,
      install_requires=[
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
