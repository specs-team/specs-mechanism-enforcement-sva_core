from peewee import *

database = PostgresqlDatabase(
    'sva',
    user='sva',
    password='sva',
    host='localhost',
)


class BaseModel(Model):
    class Meta:
        database = database


class Settings(BaseModel):
    django_url = CharField(null=True)
    event_hub_url = CharField(null=True)
    current_repository = CharField(null=True)
    sla_id = CharField(null=True)
    component_id = CharField(null=True)
