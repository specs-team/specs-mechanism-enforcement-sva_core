from models import Settings
import settings
import requests


def init_database():
    Settings.create_table(fail_silently=True)
    try:
        Settings.get()
    except Settings.DoesNotExist:
        Settings.create(django_url=None, event_hub_url=None,
                        current_repository=settings.OVAL_REPOSITORIES[0])


def send_post(url, data=None, files=None):
    try:
        response = requests.post(url, data=data, files=files, timeout=10).status_code
    except Exception:
        print "ERROR SENDING TO: %s" % url
        response = 404
    return response


def set_dashboard_url(url):
    settings_object = Settings.get()
    settings_object.django_url = url
    settings_object.save()


def set_event_hub_url(url):
    settings_object = Settings.get()
    settings_object.event_hub_url = url
    settings_object.save()


def set_sla_id(sla_id):
    settings_object = Settings.get()
    settings_object.sla_id = sla_id
    settings_object.save()


def set_component_id(component_id):
    settings_object = Settings.get()
    settings_object.component_id = component_id
    settings_object.save()


def is_dashboard_set():
    settings_object = Settings.get()
    if settings_object.django_url:
        return True
    return False


def is_event_hub_set():
    settings_object = Settings.get()
    if settings_object.event_hub_url:
        return True
    return False
