import os
OVAL_REPOSITORIES = ['http://ftp.suse.com/pub/projects/security/oval/',
                     'https://support.novell.com/security/oval/']

# Enforcement Settings

OVAL_DIR = '/tmp/ovals'
REPORT_FILES = '/tmp/'
WORKING_DIRECTORY = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
RECIPES = os.path.join(WORKING_DIRECTORY, 'recipes/')
SCANNING_REPORT = 'scanning_report_post/'
UPGRADE_REPORT = 'upgrade_report_post/'
OVAL_REPORT = 'oval_report_post/'
PLATFORM = 'openSUSE 13.1'
REFRESH_RATE = 3600

# Monitoring Settings

SVA_INFORMATION = 'sva_information/'
SCAN_REPORT = REPORT_FILES + PLATFORM + '/results.xml'
VULNERABILITY_LIST = REPORT_FILES + PLATFORM + '/oval.xml'
UP_REPORT = REPORT_FILES + PLATFORM + '/available_updates.json'
REMEDIATION_EVENT = 'remediation_event'
